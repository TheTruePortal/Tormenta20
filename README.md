# Tormenta20

This is an UNOFICIAL system made by fan and has no affiliation with Tormenta20 or Jambo Editora.
Tormenta 20 is a brazilian RPG system owned by Jambo Editora.

## Description

Build campaigns in the Tormenta20 using Foundry VTT!

## TODOS & UPDATES

* ~~Corrigir caixa de magias preparadas~~ (v0.9.2)
* ~~Adicionar novos ofícios e novas perícias~~ (v0.9.3)
* ~~Adicionar aba de biografia e imagem~~ (v0.9.3)
* ~~Equipar e desequipar itens~~ (v0.9.4)
* ~~Correção de bugs~~ (v0.9.4)
* ~~Arrumar macros~~ (v0.9.5)
* ~~Ficha de NPCS~~ (v0.9.6)
* ~~Separar equipamentos em armas, consumiveis e tesouro~~ (v0.9.7)
* ~~Melhorar ataques~~ (v0.9.7)
* ~~Aplicar dano/cura~~ (v0.9.7)
* ~~Correção de medição de distância diagonal~~ (v0.9.8)
* ~~Correções para atualização 0.7.5 do Foundry~~ (v0.9.82)
* ~~Correções para atualização 0.7.8 do Foundry~~ (v0.9.84)
* Melhorar macros, permitir alterar um ataque pela macro sem duplicar o ataque, etc
* Melhorar magias, dar suporte a aprimoramentos, etc
* Layout da ficha

## Colaboradores
* Roberto Caetano
* TheTruePortal

## Special Thanks

Special Thanks to Asacolips for his System Development Tutorial and DungeonWorld system, and Furyspark for Pathfinder 1 system which I used as reference during the development of this system.